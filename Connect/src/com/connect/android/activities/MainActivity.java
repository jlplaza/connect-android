package com.connect.android.activities;


import java.util.List;

import android.annotation.SuppressLint;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.connect.android.R;
import com.connect.android.dao.DaoMaster;
import com.connect.android.dao.DaoMaster.DevOpenHelper;
import com.connect.android.dao.DaoSession;
import com.connect.android.dao.User;
import com.connect.android.dao.UserDao;
import com.connect.android.tools.UserImage;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

public class MainActivity extends FragmentActivity
        implements
        OnMarkerClickListener,
        OnInfoWindowClickListener,
        OnMarkerDragListener,
        OnSeekBarChangeListener {
	
	private SQLiteDatabase db;
    private DaoMaster daoMaster;
    private DaoSession daoSession;
    private UserDao userDao;  
    private GoogleMap mMap;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);

        DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "users-db", null);
        db = helper.getWritableDatabase();
        daoMaster = new DaoMaster(db);
        daoSession = daoMaster.newSession();
        userDao = daoSession.getUserDao();
    }
    @Override
    protected void onStart() {
        super.onStart();
        pushUsers();
        setUpMapIfNeeded();
    }
    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }
    
    private void pushUsers(){
    	try {
	    	this.userDao.insert(new User(1L, "John Carter", -27.47093, 153.0235, "http://i.imgur.com/CxRoy.png"));
	    	this.userDao.insert(new User(2L, "Tony Stark", -33.87365, 151.20689, "http://static.tumblr.com/u7rwwlh/nyNm6v1n7/tonyavatar.png"));
	    	this.userDao.insert(new User(3L, "Linda Blair", -34.92873, 138.59995, "http://www.popcorntaxi.com.au/wp-content/uploads/2013/10/Linda-Blair.jpg"));
	    	this.userDao.insert(new User(4L, "Clark Kent", -31.952854, 115.857342, "http://www.movieneon.com/img/movies/characters/70/1668.png"));
	    	this.userDao.insert(new User(5L, "Ron Weasley", -33.87365, 138.59995, "http://www.rupertgrintpress.com/wp-content/uploads/2011/07/Unbenannt2.png"));
	    	this.userDao.insert(new User(6L, "Sam Trammell", -37.81319, 144.96298, "http://www.pagetopremiere.com/wp-content/uploads/2013/08/Screen-Shot-2013-08-14-at-3.02.35-PM.png"));
    	} catch (Exception e) {
    		System.err.println(e.toString());
    	}
    }
	
    class CustomInfoWindowAdapter implements InfoWindowAdapter {
        private final View mWindow;
        private final View mContents;

        CustomInfoWindowAdapter() {
            mWindow = getLayoutInflater().inflate(R.layout.custom_info_window, null);
            mContents = getLayoutInflater().inflate(R.layout.custom_info_contents, null);
        }
        
        @Override
        public View getInfoWindow(Marker marker) {
            render(marker, mWindow);
            return mWindow;
        }

        @Override
        public View getInfoContents(Marker marker) {
            render(marker, mContents);
            return mContents;
        }

        private void render(Marker marker, View view) {
            int badge = 0;
            ((ImageView) view.findViewById(R.id.badge)).setImageResource(badge);

            String title = marker.getTitle();
            TextView titleUi = ((TextView) view.findViewById(R.id.title));
            if (title != null) {
                SpannableString titleText = new SpannableString(title);
                titleText.setSpan(new ForegroundColorSpan(Color.DKGRAY), 0, titleText.length(), 0);
                titleUi.setText(titleText);
            } else {
                titleUi.setText("");
            }

            String snippet = marker.getSnippet();
            TextView snippetUi = ((TextView) view.findViewById(R.id.snippet));
            if (snippet != null && snippet.length() > 12) {
                SpannableString snippetText = new SpannableString(snippet);
                snippetText.setSpan(new ForegroundColorSpan(Color.MAGENTA), 0, 10, 0);
                snippetText.setSpan(new ForegroundColorSpan(Color.BLUE), 12, snippet.length(), 0);
                snippetUi.setText(snippetText);
            } else {
                snippetUi.setText("");
            }
        }
    }
    
    
   
    
    public List<User> getUsersList() {
    	return this.userDao.loadAll();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        addMarkersToMap();

        mMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);
        mMap.setOnMarkerDragListener(this);

        final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
        if (mapView.getViewTreeObserver().isAlive()) {
            mapView.getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
                @SuppressWarnings("deprecation") 
                @SuppressLint("NewApi") 
                @Override
                public void onGlobalLayout() {
                	LatLngBounds.Builder builder = new LatLngBounds.Builder();
                    for (User u: getUsersList()) {
                    	builder.include(new LatLng(u.getLatitude(), u.getLongitude()));
                    }
                    LatLngBounds bounds = builder.build();
                            
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                      mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    } else {
                      mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    }
                    mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 50));
                }
            });
        }
    }

    private void addMarkersToMap() {
    	List<User> users = getUsersList();
    	for (User u: users) {
    		new UserImage(u, u.getImage(), mMap);
    	}
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        return false;
    }

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
		
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
		
	}

	@Override
	public void onMarkerDrag(Marker arg0) {
		
	}

	@Override
	public void onMarkerDragEnd(Marker arg0) {
		
	}

	@Override
	public void onMarkerDragStart(Marker arg0) {
		
	}

	@Override
	public void onInfoWindowClick(Marker arg0) {
		
	}
}
