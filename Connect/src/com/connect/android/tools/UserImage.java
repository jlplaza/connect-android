package com.connect.android.tools;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;

import com.connect.android.dao.User;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class UserImage {
	public final int IMAGE_RADIO = 100;
	private Bitmap bitmap;
	private String url;
	private User user;
	private GoogleMap map;
	
	public UserImage(User user, String url, GoogleMap map) {
		this.url = url;
		this.user = user;
		this.map = map;
		new FileDownloader().execute(this);
	}
	
	public Bitmap getBitmap() {
		return this.bitmap;
	}

	public GoogleMap getMap() {
		return this.map;
	}
	
	public String getUrl() {
		return this.url;
	}
	
	public User getUser() {
		return this.user;
	}
	
	private class FileDownloader extends AsyncTask<UserImage, Void, UserImage> {
    	
        protected UserImage doInBackground(UserImage... userImages) {
        	UserImage userImage = userImages[0];
        	URL url;
			try {
				url = new URL(userImage.getUrl());
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return null;
			}
        	HttpURLConnection conn;
			try {
				conn = (HttpURLConnection) url.openConnection();
				conn.setDoInput(true);
	        	conn.connect();
	        	InputStream is = conn.getInputStream();
	        	Bitmap bitmap = BitmapFactory.decodeStream(is);
	        	bitmap = ImageTools.getResizedBitmap(bitmap, IMAGE_RADIO*2, IMAGE_RADIO*2);
	        	userImage.bitmap = ImageTools.roundBitmap(bitmap, IMAGE_RADIO);
	        	return userImage;
			} catch (IOException e) {
				e.printStackTrace();
			}   
            return null;
        }

        @SuppressWarnings("unused")
		protected void onProgressUpdate(Integer... progress) {
        }

		protected void onPostExecute(UserImage userImage) {
			User user = userImage.getUser();

			userImage.getMap().addMarker(new MarkerOptions()
				.position(new LatLng(user.getLatitude(), user.getLongitude()))
				.title(user.getName())
			    .icon(BitmapDescriptorFactory.fromBitmap(userImage.getBitmap()))
			    .anchor(0.5f, 1));
        }
    }
	
}
